<?php

/**
 * @author  wpWax
 * @since   6.7
 * @version 7.0.5.2
 */

use \Directorist\Helper;

if (!defined('ABSPATH')) exit;
?>

<div class="directorist-single-info directorist-single-info-phone2">

    <div class="directorist-single-info__value">
        <a href="https://wa.me/<?php Helper::formatted_tel($value); ?>"><img src="https://www.freepnglogos.com/uploads/whatsapp-logo-hd-18.png" width="80" /></a>
    </div>

</div>