<?php

/**
 * @author  wpWax
 * @since   6.7
 * @version 6.7
 */

if (!defined('ABSPATH')) exit;

?>

<div class="directorist-single-info directorist-single-info-text">

    <?php if (isset($data['field_key']) && $data['field_key'] !== 'booking-shortcode') : ?>
        <div class="directorist-single-info__label"><span class="directorist-single-info__label-icon"><?php directorist_icon($icon); ?></span class="directorist-single-info__label--text"><span><?php echo esc_html($data['label']); ?></span></div>
    <?php endif; ?>
    <div class="directorist-single-info__value">
        <?php
        if (isset($data['field_key']) && $data['field_key'] == 'booking-shortcode') :
            echo do_shortcode($value);
        else :
            echo esc_html($value);
        endif;
        ?>
    </div>

</div>