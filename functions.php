<?php
// Load child stylesheet

add_action('wp_enqueue_scripts', 'dlist_child_theme_styles');
function dlist_child_theme_styles()
{

	if (is_rtl()) {
		wp_enqueue_style('child-style', get_stylesheet_directory_uri() . '/style.css', array('dlist-style'), null);
	} else {
		wp_enqueue_style('parent-style', get_parent_theme_file_uri('/style.css'));
	}
}

// MAKE FIELD KEY VISIBLE

add_filter('directorist_custom_field_meta_key_field_args', function ($args) {
	$args['type'] = 'text';
	return $args;
});

// MEasurment Unit Text

if (!function_exists('measurment_unit_text')) {
	function measurment_unit_text($unit = 0, $measurment_unit = 'piece')
	{
		if ($unit == 0) return '';

		$s = $unit > 1 ? false : true;
		$text = 'Piece';
		switch ($measurment_unit) {
			case 'piece':
				$text = $s ? 'Piece' : 'Pieces';
				break;
			case 'dozen':
				$text = $s ? 'Dozen' : 'Dozens';
				break;
			case 'kg':
				$text = $s ? 'KG' : 'KG';
				break;
			case 'liter':
				$text = $s ? 'Liter' : 'Liters';
				break;
			default:
				$text = $measurment_unit;
				break;
		}
		return $text;
	}
}

//Signup Modal Manipulation
add_action('wp_footer', function () {
?>
	<script>
		jQuery(document).ready(function() {
			jQuery('#vb_username').attr('placeholder', 'Username/Business ( eg. shopzone )')
		})
	</script>
<?php
});

// change footer to DARK version
add_action('wp_footer', function () {
?>
	<script>
		jQuery(document).ready(function() {
			jQuery('footer').removeClass().addClass('footer-three');
		})
	</script>
<?php
});

//Mobile Chat View
add_action('wp_footer', function () {
?>
	<script>
		jQuery(document).ready(function() {
			adjust_chat_wrapper_height()
			jQuery(window).resize(function() {
				adjust_chat_wrapper_height()
			});
		})

		function adjust_chat_wrapper_height() {
			var width = jQuery(window).width();
			var height = jQuery(window).height();
			var chatHeight = (height - 146);
			var maxHeight = 625;
			if (chatHeight > 625) maxHeight = chatHeight;
			if (width < 600) {
				jQuery('#directorist-user-message-container ul#directorist-user-message-box').css({
					'height': chatHeight,
					'max-height': maxHeight
				});
			}
		}
	</script>
<?php
});


//add_filter('manage_edit-post_columns', 'taxonomy_filter_manage_bulk_columns', 99, 1);
//add_filter('manage_edit-at_biz_dir-category_columns', 'taxonomy_filter_manage_bulk_columns', 99, 1);
//add_filter('manage_edit-at_biz_dir-location_columns', 'taxonomy_filter_manage_bulk_columns', 99, 1);


// AFFITIATE

add_filter('yith_wcaf_payment_email_required', function( $required ){return false;});

add_action('yith_wcaf_register_form', function(){
?>
	<p class="form-row form-row-wide">
		<label for="occupation"><?php esc_html_e( 'Occupation', 'yith-woocommerce-affiliates' ); ?></label>
		<input type="text" class="input-text" name="occupation" id="occupation" value="<?php echo ! empty( $_POST['occupation'] ) ? esc_attr( wp_unslash( $_POST['occupation'] ) ) : ''; ?>" />
	</p>
<?php
});

add_action('yith_wcaf_new_affiliate', function($id){
	if(isset($_POST['occupation']) && !empty($_POST['occupation'])){
		update_user_meta($id, 'occupation', $_POST['occupation']);
	}
});


add_action('wp_footer', function(){
	if( is_user_logged_in() ) {
		$user = wp_get_current_user();
		$roles = ( array ) $user->roles;
		if( in_array( 'yith_affiliate', $roles ) ){
?>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery('.author__access_area .author-info ul').html('<li><a href="/my-account/">My Account</a></li><li><a href="/affiliate-dashboard/">Affiliate Dashboard</a></li><li><a href="<?php echo wp_logout_url(home_url());?>">Logout</a></li>');
		});
	</script>
<?php
		}
	}
});